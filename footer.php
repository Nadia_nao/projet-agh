<footer class="footer">
    <ul>
        <li><a href="https://www.facebook.com/" target="_blank">
             <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://www.twitter.com" target="_blank">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://plus.google.com/" target="_blank">
             <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://www.linkedin.com" target="_blank">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
    </ul>

    <p>Place du Temps Libre 6 - 1200 Woluwe-St-Lambert</p>
    <p>©2016 All rights reserved to AGH</p>
</footer>