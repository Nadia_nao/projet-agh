<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Projets</title>
        <link rel="stylesheet" href="./css/normalize.css">
<!--        FontAwesome-->
        <link rel="stylesheet" href="./css/font-awesome.min.css">
<!--        Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet">
<!--        Hamburger-->
        <link rel="stylesheet" href="./css/hamburger.css">
<!--        jQuery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!--        plug-in-->
        <script type='text/javascript' src='./unitegallery/js/unitegallery.min.js'></script>
        <link rel='stylesheet' href='./unitegallery/css/unite-gallery.css' type='text/css' />
        <script type='text/javascript' src='./unitegallery/themes/tiles/ug-theme-tiles.js'></script>
<!--       Nos fichiers-->
        <script src="./js/scriptPlugInProjet.js"></script>
        <script src="./js/script.js"></script>
        <link rel="stylesheet" href="./css/screen.css">
        
    </head>
    <body>
        <?php
            require_once("./header.php");
        ?>
        
        
        <nav class="nav">
            <a href=""><i class="fa fa-times fa-2x"></i></a>
            <ul>
                <li><a href="bureau.php">le bureau</a></li>
                <span><li><a href="projet.php">projets</a></li></span>
                <li><a href="architectureVerte.php">architecture verte</a></li>
                <li><a href="autourDeLaMaison.php">autour de la maison</a></li>
                <li><a href="tendances.php">tendances</a></li>
            </ul>
        </nav>
        <main class="projet">
            <h1>projets</h1>
            <ul>
                <li><a href="#block1" id="link1" class="link isActiveTendance" onclick="openTab(event, 'block1')">Broqueville</a></li>
                <li><a href="#block2" id="link2" class="link" onclick="openTab(event, 'block2')">Collège Jean XXIII</a></li>
                <li><a href="#block3" id="link3" class="link" onclick="openTab(event, 'block3')">Hippocrate</a></li>
            </ul>
        
            <section id="block1" class="content">
               <p>Rénovation et extension d'une habitation mitoyenne - Woluwe-St-Lambert</p>
                <div id="gallery1" style="display:none;">


                    <img alt="Façade"
                         src="./images/photos/projet01/img03.jpg"
                         data-description="Isolation paille"
                         style="display:none">
                    
                    <img alt="Détail façade"
                         src="./images/photos/projet01/img01.jpg"
                         data-description="Matériaux"
                         style="display:none">
                    
                    <img alt="Toiture végétale"
                         src="./images/photos/projet01/img05.jpg"
                         data-description="Ossature bois"
                         style="display:none">
                    
                    
                    <img alt="Escalier bois"
                         src="./images/photos/projet01/img02.jpg"
                         data-description="Mur végétal"
                         style="display:none">
                         
                    <img alt="Aménagement intérieur"
                         src="./images/photos/projet01/img06.jpg"
                         data-description="Mur végétal"
                         style="display:none">


                </div>
            </section>
            
            <section id="block2"  class="content">
                <p>Projet école passive - Woluwe-St-Pierre</p>
                <div id="gallery2" style="display:none;">

                    <img alt="Façade principale"
                         src="./images/photos/projet02/img01.jpg"
                         data-description="Façade principale"
                         style="display:none">
                    
                    <img alt="Entrée pavillon"
                         src="./images/photos/projet02/img04.jpg"
                         data-description="Entrée pavillon"
                         style="display:none">
                         
                    <img alt="Intérieur vue escalier"
                         src="./images/photos/projet02/img02.jpg"
                         data-description="Intérieur vue escalier"
                         style="display:none">
                    
                    <img alt="Intérieur vue couloir"
                         src="./images/photos/projet02/img03.jpg"
                         data-description="Intérieur vue couloir"
                         style="display:none">
                         
                    <img alt="Vue aérienne"
                         src="./images/photos/projet02/img05.jpg"
                         data-description="Vue aérienne"
                         style="display:none">

                </div>
            </section>
            
            <section id="block3"  class="content">
                <p>Construction d'une habitation passive - Zaventem</p>
                <div id="gallery3" style="display:none;">

                    <img alt="Modélisation 3D"
                         src="./images/photos/projet03/img05.jpg"
                         data-description="Modélisation 3D"
                         style="display:none">
                    
                    <img alt="Modélisation 3D RDC"
                         src="./images/photos/projet03/img04.jpg"
                         data-description="Modélisation 3D RDC"
                         style="display:none">
                         
<!--
                    <img alt="Ventilation double flux"
                         src="./images/photos/projet03/img03.jpg"
                         data-description="Ventilation double flux"
                         style="display:none">
-->
                    
                    <img alt="Modélisation 3D 1er étage"
                         src="./images/photos/projet03/img02.jpg"
                         data-description="Modélisation 3D 1er étage"
                         style="display:none">
                         
                    <img alt="Réalisation"
                         src="./images/photos/projet03/img01.jpg"
                         data-description="Réalisation"
                         style="display:none">

                </div>
            </section>
        </main>
        
        <?php
            require_once("./footer.php");
            require_once("./contact.php");
        ?>
        

    </body>
</html>