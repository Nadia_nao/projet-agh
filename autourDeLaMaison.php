<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Autour de la maison</title>
        <link rel="stylesheet" href="./css/normalize.css">
<!--        FontAwesome-->
        <link rel="stylesheet" href="./css/font-awesome.min.css">
<!--        Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet">
<!--        Hamburger-->
        <link rel="stylesheet" href="./css/hamburger.css">
<!--        jQuery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!--       Nos fichiers-->
        <script src="./js/script.js"></script>
        <link rel="stylesheet" href="./css/screen.css">
        
    </head>
    <body>
        <?php
            require_once("./header.php");
        ?>
        
        
        <nav class="nav">
            <a href=""><i class="fa fa-times fa-2x"></i></a>
            <ul>
                <li><a href="bureau.php">le bureau</a></li>
                <li><a href="projet.php">projets</a></li>
                <li><a href="architectureVerte.php">architecture verte</a></li>
                <span><li><a href="autourDeLaMaison.php">autour de la maison</a></li></span>
                <li><a href="tendances.php">tendances</a></li>
            </ul>
        </nav>
        <main class="adlm">
            <h1>autour de la maison</h1>
            <section class="picto">
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://www.environnement.brussels/thematiques/energie/primes-et-incitants" target="_blank">Bruxelles</a></p>
                        <p><a href="http://www.energiesparen.be/subsidies/particulieren" target="_blank">Flandre</a></p>
                        <p><a href="http://energie.wallonie.be/fr/primes.html?IDC=7015" target="_blank">Wallonie</a></p>
                    </div>
                </section>
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://www.guide-epargne.be/epargner/comparer/emprunt-hypothecaire.html" target="_blank">FR</a></p>
                        <p><a href="http://www.spaargids.be/sparen/vergelijk/hypothecaire-leningen.html" target="_blank">NL</a></p>
                    </div>
                </section>
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://batibouw.be/fr/" target="_blank">Batibouw</a></p>
                        <p><a href="http://www.bois-habitat.be/" target="_blank">Bois &amp; Habitat</a></p>
                        <p><a href="http://www.energie-habitat.be/" target="_blank">Energie &amp; Habitat</a></p>
                    </div>
                </section>
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://www.mon-habitat-durable.fr/" target="_blank">Se renseigner</a></p>
                    </div>
                </section>
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://www.materiaux-ecologiques.com/welcome.html" target="_blank">S'informer</a></p>
                    </div>
                </section>
                <section>
                    <article></article>
                    <div>
                        <p><a href="http://www.ordredesarchitectes.be/fr-be/" target="_blank">FR</a></p>
                        <p><a href="http://architect.be/" target="_blank">NL</a></p>
                    </div>
                </section>
            </section>
        </main>
        
        <?php
            require_once("./footer.php");
            require_once("./contact.php");
        ?>
        

    </body>
</html>