<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Le bureau</title>
        <link rel="stylesheet" href="./css/normalize.css">
<!--        FontAwesome-->
        <link rel="stylesheet" href="./css/font-awesome.min.css">
<!--        Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet">
<!--        Hamburger-->
        <link rel="stylesheet" href="./css/hamburger.css">
<!--        jQuery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!--       Nos fichiers-->
        <script src="./js/script.js"></script>
        <link rel="stylesheet" href="./css/screen.css">
        
    </head>
    <body>
        <?php
            require_once("./header.php");
        ?>
        
        
        <nav class="nav">
           <a href=""><i class="fa fa-times fa-2x"></i></a>
            <ul>
                <span><li><a href="bureau.php">le bureau</a></li></span>
                <li><a href="projet.php">projets</a></li>
                <li><a href="architectureVerte.php">architecture verte</a></li>
                <li><a href="autourDeLaMaison.php">autour de la maison</a></li>
                <li><a href="tendances.php">tendances</a></li>
            </ul>
        </nav>
        <main class="bureau">
            <h1>le bureau</h1>
            <section>
                <p>A.G.H, dernières ces trois lettres, trois architectes qui ont décidé de repenser le monde.</p>
                <p>Ou plutôt votre monde. Et ce, de manière écologique et durable. Fortes de 10 ans d’expériences dans divers bureaux d’étude, nous avons rassemblé nos compétences qui vont de l’architecture pure à l’aménagement des espaces extérieurs afin de créer notre bureau prônant une vision commune : Une architecture écologique d’un style épuré.</p>
                <p>L’objectif n’est pas simplement de surfer sur la tendance. Il s’agit d’une autre manière de vivre et penser l’habitat; idéal pour votre bien être, votre budget et notre planète. </p>
                <p>Construction, rénovation, extension, réaménagement de l’espace et des abords. Tout est pensé et repensé dans un esprit vert et durable.</p>
                <p>Nous sommes à votre écoute pour réaliser votre projet green rêvé, tout en respectant votre budget.</p>
            </section>
            <section>
                <section>
                    <div></div>
                    <article>
                        <h3>Nadia Ajaji</h3>
                        <ul>
                            <li>Spécialisée |</li>
                            <li>ossature en bois et isolation verte</li>
                        </ul>
                        <p><cite>"La simplicité est dans la sophistication suprême."</cite></p>
                        <p>Léonardo Da Vinci</p>
                    </article>
                </section>
                <section>
                    <div></div>
                    <article>
                        <h3>Natacha Verheyden</h3>
                        <ul>
                            <li>Spécialisée |</li>
                            <li>aménagement de l'espace intérieur écologique</li>
                        </ul>
                        <p><cite>"L'essence d'un projet, c'est l'harmonie parfaite entre le beau, l'utile et le juste."</cite></p>
                        <p>F.L.Wright</p>
                    </article>
                </section>
                <section>
                    <div></div>
                    <article>
                        <h3>Magda Mirica</h3>
                        <ul>
                            <li>Spécialisée |</li>
                            <li>murs et toitures végétales</li>
                        </ul>
                        <p><cite>"Les détails font la perfection et la perfection n'est pas un détail."</cite></p>
                        <p>Léonardo Da Vinci</p>
                    </article>
                </section>
            </section>
        </main>
        
        <?php
            require_once("./footer.php");
            require_once("./contact.php");
        ?>
        
        
        

    </body>
</html>