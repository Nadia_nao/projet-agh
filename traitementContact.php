<?php

    include "./config/db.php";
        
        
    try {
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.
                ';dbname='.DBNAME.';charset='
                .DBCHARSET,DBUSER,DBPASS); 
    }
    catch (Exception $e){
        die ('Une erreur sest produite ');
    }

    //les variables
    $nom = $_POST['nom'];
    $email = $_POST['email'];
    $num = $_POST['num'];
    $sujet = $_POST['sujet'];
    $msg = $_POST['msg'];


//vérifier et compiler les champs vide dans array
$champsVides=[];
foreach ($_POST as $key=>$value){
    if (empty ($value)) {
        $champsVides[]=$key;
     }
}

//envoyer ou message erreur
if (count($champsVides)==0){

    $sql = "INSERT INTO contact(id, nom, email, num, sujet, msg) VALUES (null, :nom, :email, :num, :sujet, :msg)";
    $statement = $bdd->prepare($sql);

    $statement->bindValue(":nom", $nom);
    $statement->bindValue(":email", $email);
    $statement->bindValue(":num", $num);
    $statement->bindValue(":sujet", $sujet);
    $statement->bindValue(":msg", $msg);

    if ($statement->execute()){
        echo $nom.", votre message a été envoyé ";    
    }
    else{
        die ('Une erreur sest produite ');
    }
        
}
else{
    
    $listeChamps= implode (", ",$champsVides);
    echo "Vous devez remplir les champs suivants: ".$listeChamps;

    
}

?>