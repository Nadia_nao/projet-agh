<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Archirecture verte</title>
        <link rel="stylesheet" href="./css/normalize.css">
<!--        FontAwesome-->
        <link rel="stylesheet" href="./css/font-awesome.min.css">
<!--        Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet">
<!--        Hamburger-->
        <link rel="stylesheet" href="./css/hamburger.css">
<!--        jQuery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!--       Nos fichiers-->
        <script src="./js/script.js"></script>
        <link rel="stylesheet" href="./css/screen.css">
        
    </head>
    <body>
        <?php
            require_once("./header.php");
        ?>
        
        
        <nav class="nav">
            <a href=""><i class="fa fa-times fa-2x"></i></a>
            <ul>
                <li><a href="bureau.php">le bureau</a></li>
                <li><a href="projet.php">projets</a></li>
                <span><li><a href="architectureVerte.php">architecture verte</a></li></span>
                <li><a href="autourDeLaMaison.php">autour de la maison</a></li>
                <li><a href="tendances.php">tendances</a></li>
            </ul>
        </nav>
        <main class="archiverte">
            <h1>architecture verte</h1>
            <section>
                <p>L’architecture écologique (ou architecture durable) est un mode de conception et de réalisation ayant pour préoccupation de concevoir une architecture respectueuse de l’environnement et de l’écologie. Il existe de multiples facettes de l’architecture verte, certaines s’intéressant surtout à la technologie, la gestion, ou d’autres privilégient la santé de l’homme, ou encore d’autres, plaçant le respect de la nature au centre de leurs préoccupations.</p>
                <img src="./images/photos/archiVerte.jpg" alt="architecture verte">
                <p>On peut distinguer plusieurs « lignes directrices » :</p>
                <ul>
                    <li>Le choix des matériaux, naturels et respectueux de la santé de l’homme.</li>
                    <li>Le choix de la disposition des pièces (par exemple) pour favoriser les économies d’énergie en réduisant les besoins énergétiques.</li>
                    <li>Le choix des méthodes d’apports énergétiques.</li>
                    <li>Le choix du cadre de vie offert ensuite à l’homme (jardin, etc.)</li>
                </ul>   
                <p>Le but primordial de l’architecture durable est l’efficacité énergétique de la totalité du cycle de vie d’un bâtiment. Les architectes utilisent de nombreuses techniques différentes pour réduire les besoins énergétiques de bâtiments, et ils augmentent leur capacité à capturer ou générer leur propre énergie.
                Les matériaux durables qui sont utilisés pour l’isolation sont des denims recyclés, en laine de verre, la paille, le bois, la laine de mouton, la ouate de cellulose…
                Certaines architectures durables incorporent des matériaux recyclés ou de seconde main. La réduction de l’emploi de matériaux nouveaux correspond à une réduction de l’énergie grise (c’est-à-dire l’énergie utilisée pour produire les matériaux). Souvent les architectes écologiques essayent de réhabiliter de vieux bâtiments afin qu’ils remplissent les besoins des nouveaux usages, et éviter ainsi de nouvelles constructions pas forcément nécessaires.</p>
            </section>
        </main>
        
        <?php
            require_once("./footer.php");
            require_once("./contact.php");
        ?>
        

    </body>
</html>