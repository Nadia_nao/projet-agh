/*===================================CONTACT==========================================*/
document.addEventListener("DOMContentLoaded", function(){
    if(document.documentElement.clientWidth < 720){
        var btnContact = document.querySelector("header li:nth-of-type(2) a");
        var btnClose = document.querySelector(".contact li a");
        var panelContact = document.querySelector(".contact");


        btnContact.addEventListener("click", function(evt){
            //pour empêcher comportement du a
            evt.preventDefault();
            panelContact.style.right = 0;
            panelContact.style.opacity = 1;
        });

        btnClose.addEventListener("click", function(evt){
            evt.preventDefault();
            panelContact.style.right = '-45em';
            panelContact.style.opacity = 0.5;
        });
    }
    else{
        var btnContact = document.querySelector("header li:nth-of-type(2) a");
        var btnClose = document.querySelector(".contact li a");
        var panelContact = document.querySelector(".contact");


        btnContact.addEventListener("click", function(evt){
            //pour empêcher comportement du a
            evt.preventDefault();
            panelContact.style.right = 0;
            panelContact.style.opacity = 1;
        });

        btnClose.addEventListener("click", function(evt){
            evt.preventDefault();
            panelContact.style.right = '-100em';
            panelContact.style.opacity = 0.5;
        });
    }
});



/*===================================NAV/MOBILE==========================================*/
document.addEventListener("DOMContentLoaded", function(){
        var btnMenu = document.querySelector("header>a");
        var btnClose = document.querySelector(".fa-times");
        var panelMenu = document.querySelector("nav");


        btnMenu.addEventListener("click", function(evt){
            //pour empêcher comportement du a
            evt.preventDefault();
            panelMenu.style.left = 0;
            panelMenu.style.opacity = 1;
        });

        btnClose.addEventListener("click", function(evt){
            evt.preventDefault();
            panelMenu.style.left = '-45em';
            panelMenu.style.opacity = 0.5;
        });
});


/*===================================TENDANCES==========================================*/
//function openTab(evt, block){
////    var elem = document.querySelector("main");
////    var theCSSprop = window.getComputedStyle(elem, null).getPropertyValue("position");
////    console.log(theCSSprop);
//    
//    var content = document.getElementsByClassName("content");
//   var link = document.getElementsByClassName("link");
//
//   for(var i = 0; i < content.length; i++){
//       content[i].style.display = "none";
//   };
//
//   for(var i = 0; i < link.length; i++){
//        link[i].classList.remove("isActiveTendance");
//   };
//
//   document.getElementById(block).style.display = "block";
//   evt.currentTarget.classList.add("isActiveTendance");   
//};


/*===================================AUTOUR/MAISON==========================================*/
$(document).ready(function(){
    // vider les champs
    // ...
    
    
    $('.picto article').click(function(){
        $('.picto div').slideUp("slow");
        $(this).next().slideDown("slow");
//                    $(this).next().delay(3000).slideUp("slow");
        console.log(this);
    });

    $('.picto').mouseleave(function(){
        $('.picto div').slideUp("slow");
    });

/*===================================CONTACT/FORMULAIRE==========================================*/
    
    
    $("#btnContact").click (function(){
        $.ajax({
            url: "./traitementContact.php",
            type: 'POST',
            data: {
                nom: $('#nom').val(),
                email: $('#email').val(),
                num: $('#num').val(),
                sujet: $('#sujet').val(),
                msg: $('#msg').val()
            },
            success: function (donnees){
//                console.log("log: "+donnees);
                $("#resulEnvoi").css("display", "block").html(donnees);
                $("input","textearea").value = '';

            }
        });

    });
});


