jQuery(document).ready(function(){
//    var plugInGallery;
    $('.link').click(function(evt){
        evt.preventDefault();
        var currentHash = this.hash;
        
        $('.link').removeClass("isActiveTendance");
        $(this).addClass("isActiveTendance");
        
        $('.tendance section').css("display", "none");
        $(currentHash).css("display", "block");
        
//        plugInGallery = jQuery("#gallery").unitegallery(currentHash);
    });
    

    jQuery("#gallery1").unitegallery({
        tile_enable_shadow:true,
        tile_shadow_color:"#8B8B8B",
        tile_overlay_opacity:0.3,
        tile_as_link:true,
        tiles_col_width:230,
        tiles_space_between_cols:20,
        tile_enable_textpanel:true,
        tile_textpanel_title_text_align: "center",
        tile_textpanel_always_on:true,
    });
    
     jQuery("#gallery2").unitegallery({
        tile_enable_shadow:true,
        tile_shadow_color:"#8B8B8B",
        tile_overlay_opacity:0.3,
        tile_as_link:true,
        tiles_col_width:360,
        tiles_space_between_cols:20,
        tile_enable_textpanel:true,
        tile_textpanel_title_text_align: "center",
        tile_textpanel_always_on:true,
    });

});