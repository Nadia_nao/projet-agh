<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Tendances</title>
        <link rel="stylesheet" href="./css/normalize.css">
<!--        FontAwesome-->
        <link rel="stylesheet" href="./css/font-awesome.min.css">
<!--        Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,900" rel="stylesheet">
<!--        Hamburger-->
        <link rel="stylesheet" href="./css/hamburger.css">
<!--        jQuery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!--        plug-in-->
        <script type='text/javascript' src='./unitegallery/js/unitegallery.min.js'></script>
	    <link rel='stylesheet' href='./unitegallery/css/unite-gallery.css' type='text/css' />
	    <script type='text/javascript' src='./unitegallery/themes/tiles/ug-theme-tiles.js'></script>
<!--       Nos fichiers-->
	    <script src="./js/scriptPlugInTendance.js"></script>
        <script src="./js/script.js"></script>
        <link rel="stylesheet" href="./css/screen.css">
        
    </head>
    <body>
        <?php
            require_once("./header.php");
        ?>
        
        
        <nav class="nav">
            <a href=""><i class="fa fa-times fa-2x"></i></a>
            <ul>
                <li><a href="bureau.php">le bureau</a></li>
                <li><a href="projet.php">projets</a></li>
                <li><a href="architectureVerte.php">architecture verte</a></li>
                <li><a href="autourDeLaMaison.php">autour de la maison</a></li>
                <span><li><a href="tendances.php">tendances</a></li></span>
            </ul>
        </nav>
        <main class="tendance">
            <h1>tendances</h1>
            <ul>
                <li><a href="#block1" id="link1" class="link isActiveTendance" onclick="openTab(event, 'block1')">Actuelles</a></li>
                <li><a href="#block2" id="link2" class="link" onclick="openTab(event, 'block2')">Visionnaires</a></li>
            </ul>
        
            <section id="block1" class="content">
                <div id="gallery1" style="display:none;">

                    <a href="https://www.engie-electrabel.be/fr/particulier/energy-manager/isolation/article?article=152-Isoler-maison-avec-paille">
                    <img alt="Isolation paille"
                         src="./images/photos/actuelle2.jpg"
                         data-description="Isolation paille"
                         style="display:none">
                    </a>
                    
                    <a href="http://www.ecologikmagazine.fr/realisations-durables-c15/">
                    <img alt="Matériaux"
                         src="./images/photos/actuelle3.jpg"
                         data-description="Matériaux"
                         style="display:none">
                    </a>
                    
                    <a href="http://www.maisons-ossature-bois.be/">
                    <img alt="Ossature bois"
                         src="./images/photos/actuelle1.jpg"
                         data-description="Ossature bois"
                         style="display:none">
                    </a>
                    
                    
                    <a href="http://www.murvegetalpatrickblanc.com">
                    <img alt="Mur Végétal"
                         src="./images/photos/actuelle4.jpg"
                         data-description="Mur végétal"
                         style="display:none">
                    </a>


                </div>
            </section>
            <section id="block2"  class="content">
                <div id="gallery2" style="display:none;">

                    <a href="http://www.vegetalcity.net/">
                    <img alt="Citée végétale"
                         src="./images/photos/visionnaire1.jpg"
                         data-description="Citée végétale"
                         style="display:none">
                    </a>
                    
                    <a href="http://vincent.callebaut.org/projets-groupe-tout.html">
                    <img alt="Lily pad"
                         src="./images/photos/visionnaire2.jpg"
                         data-description="Lily pad"
                         style="display:none">
                    </a>

                </div>
        </section>
        </main>
        
        <?php
            require_once("./footer.php");
            require_once("./contact.php");
        ?>
        

    </body>
</html>