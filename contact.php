<section class="contact">   
    <section>
        <article>
            <img src="./images/logo/LogoFullBlanc.svg" alt="logoAGH">
        </article>
        <ul>
            <li><a href=""><i class="fa fa-times fa-1x"></i></a></li>
        </ul>
    </section>
    <section>
<!--        <form id="formContact" method="POST">-->
<!--                    <label for="nom">Nom</label>-->
            <input type="text" id="nom" name="nom" placeholder="Nom">
<!--                    <label for="email">Email</label>-->
            <input type="text" id="email" name="email" placeholder="Email">
<!--                    <label for="num">N° de portable</label>-->
            <input type="text" id="num" name="num" placeholder="N° de portable">
<!--                    <label for="sujet">Sujet</label>-->
            <input type="text" id="sujet" name="sujet" placeholder="Sujet">
<!--                    <label for="msg">Message</label>-->
            <textarea id="msg" name="msg" rows="6" placeholder="Votre message"></textarea>
            <input type="button" id="btnContact" value="Envoyer">
            <div id="resulEnvoi"></div>
<!--        </form>-->
    </section>
    <section>
        <p>Où nous trouvez ?</p>
        <ul>
            <li><i class="fa fa-map-marker fa-1x"></i></li>
            <li>Place du Temps Libre 6 - 1200 Woluwe-St-Lambert</li>
        </ul>
        <ul>
            <li><i class="fa fa-phone fa-1x"></i></li>
            <li>02/433.62.15</li>
        </ul>
        <ul>
            <li><i class="fa fa-fax fa-1x"></i></li>
            <li>02/436.12.84</li>
        </ul>
        <ul>
            <li><i class="fa fa-at fa-1x"></i></li>
            <li>contact@agh.be</li>
        </ul>
        <iframe src="https://www.google.com/maps/d/embed?mid=1d-5n7KKrV2DNralVGkDZFkvWC30" width="580" height="300" frameborder="0" border="0" sandbox="allow-scripts"></iframe>
    </section>
    <section>
        <ul>
        <li><a href="https://www.facebook.com/" target="_blank">
             <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://www.twitter.com" target="_blank">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://plus.google.com/" target="_blank">
             <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        <li><a href="https://www.linkedin.com" target="_blank">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
             </span>
        </a></li>
        </ul>

        <p>Place du Temps Libre 6 - 1200 Woluwe-St-Lambert</p>
        <p>©2016 All rights reserved to AGH</p>
    </section>
</section>